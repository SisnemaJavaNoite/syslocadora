package br.com.sisnema.syslocadora.view;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

//import javax.swing.JOptionPane;

public class SysLocadoraView {
	public static void main(String[] args) {
		
		SysLocadoraView.getMenu();	
		
	} // Fim main

	private static Integer getMenu() {
		int key = 77;
		
		while (key != 0 ) {
			JTextField field1 = new JTextField();
			Object[] message = {
					"1 - Clientes\n",
					"2 - Filmes\n",
					"3 - Midias\n",
					"4 - Gravar em Disco\n",
					"5 - Ler do Disco\n",
					"0 - Sair\n",
					"Opção: ", field1
			};
			int option = JOptionPane.showConfirmDialog(null, message,"Menu Principal",JOptionPane.OK_CANCEL_OPTION);
			
			if (option == JOptionPane.OK_OPTION) {
				
				key = Integer.parseInt(field1.getText());
			
				
				switch (key) {
					case 1:
						PessoaView.run();
						break;
					case 2:	
						FilmeView.run();
						break;
					case 3:	
						MidiaView.run();
						break;
					case 4:	
						break;
					case 5:	
						break;
					case 0:	
						JOptionPane.showMessageDialog(null, "Saindo");
						break;
					default:
						JOptionPane.showMessageDialog(null, key + " Opção inválida");
						break;
				} // fim Switch
				
			} else {
				key=0;
				// Fim IF Option
			}
			
		}//Fim while		
		return key;
	} // Fim GetMenu()
	
	
}  // Fim Class SysLocadoraView
