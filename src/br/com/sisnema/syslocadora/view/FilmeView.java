package br.com.sisnema.syslocadora.view;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import br.com.sisnema.syslocadora.model.Filme;



public class FilmeView {
		public static void run(){
			
			
		}
		
		private static Filme cadastrar() {
				Filme filme = new Filme();
				
				JTextField field1 = new JTextField();
				JTextField field2 = new JTextField();
				JTextField field3 = new JTextField();
				
				Object[] message = {
				    "Codigo:", field1,
				    "Genero:", field2,
				    "Nome:", field3,
				};
				
				int option = JOptionPane.showConfirmDialog(null, message, "Cadastro de Clientes", JOptionPane.OK_CANCEL_OPTION);
				
				if (option == JOptionPane.OK_OPTION) {
				    filme.codigo = field1.getText();
				    filme.nome = field2.getText();
				    filme.genero = field3.getText();
				    
				    JOptionPane.showMessageDialog(null, "Cliente cadastrado com sucesso \n" 
							+"ID: " + filme.codigo +"\n"
							+"Nome: " + filme.nome +"\n"
							+"Genero: " + filme.genero +"\n"
							);
				   
				} // Fim IF Option
				
				return filme;
				
			} // fim Class Filme Cadastrar()
		
		private static String getMenu() {
			int key = 77;
			
			while (key != 0 ) {
				JTextField field1 = new JTextField();
				Object[] message = {
						"1 - Cadastrar\n",
						"2 - Alterar\n",
						"3 - Excluir\n",
						"4 - Listar\n",
						"0 - Voltar\n",
						"Opção: ", field1
				};
				int option = JOptionPane.showConfirmDialog(null, message,"Menu Filmes",JOptionPane.OK_CANCEL_OPTION);
				
				if (option == JOptionPane.OK_OPTION) {
					
					key = Integer.parseInt(field1.getText());
				
					
					switch (key) {
					case 1:
						JOptionPane.showMessageDialog(null, "Menu Clientes");
						cadastrar();
						break;
					case 2:	
						JOptionPane.showMessageDialog(null, "Menu Alterar");
						//alterar();
						break;
					case 3:	
						JOptionPane.showMessageDialog(null, "Menu Excluir");
						//excluir();
						break;
					case 4:	
						JOptionPane.showMessageDialog(null, "Listar");
						//listar();
						break;
					case 0:	
						JOptionPane.showMessageDialog(null, "Abrindo Menu Anterior");
						break;
					default:
						JOptionPane.showMessageDialog(null, key + " Opção inválida");
						break;
					} // fim Switch
					
				} else {
					key=0;
					// Fim IF Option
				}
				
			}//Fim while
			
			return key;
			
		} //fim getMenu()
		
}
