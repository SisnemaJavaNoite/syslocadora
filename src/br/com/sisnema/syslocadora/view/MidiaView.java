package br.com.sisnema.syslocadora.view;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import br.com.sisnema.syslocadora.model.Midia;


public class MidiaView {
	public static void run(){
		cadastrar();
		getMenu();
				
	}
	
	private static Midia cadastrar() {
		Midia midia = new Midia();
		
		JTextField field1 = new JTextField();
		JTextField field2 = new JTextField();
		
		Object[] message = {
		    "Codigo:", field1,
		    "Descrição:", field2,
		};
		
		int option = JOptionPane.showConfirmDialog(null, message, "Cadastro de Clientes", JOptionPane.OK_CANCEL_OPTION);
		
		if (option == JOptionPane.OK_OPTION) {
		    midia.codigo = field1.getText();
		    midia.descricao = field2.getText();
		    
		    JOptionPane.showMessageDialog(null, "Cliente cadastrado com sucesso \n" 
					+"Codigo: " + midia.codigo +"\n"
					+"Descriçao: " + midia.descricao +"\n"
					);
		   
		} // Fim IF Option
		
		return midia;
		
	} // fim Class midia Cadastrar()
	
	private static String getMenu() {
		int key = 77;
		
		while (key != 0 ) {
			JTextField field1 = new JTextField();
			Object[] message = {
					"1 - Cadastrar\n",
					"2 - Alterar\n",
					"3 - Excluir\n",
					"4 - Listar\n",
					"0 - Voltar\n",
					"Opção: ", field1
			};
			int option = JOptionPane.showConfirmDialog(null, message,"Menu Principal",JOptionPane.OK_CANCEL_OPTION);
			
			if (option == JOptionPane.OK_OPTION) {
				
				key = Integer.parseInt(field1.getText());
			
				
				switch (key) {
				case 1:
					JOptionPane.showMessageDialog(null, "Menu Clientes");
					cadastrar();
					break;
				case 2:	
					JOptionPane.showMessageDialog(null, "Menu Alterar");
					//alterar();
					break;
				case 3:	
					JOptionPane.showMessageDialog(null, "Menu Excluir");
					//excluir();
					break;
				case 4:	
					JOptionPane.showMessageDialog(null, "Listar");
					//listar();
					break;
				case 0:	
					JOptionPane.showMessageDialog(null, "Abrindo Menu Anterior");
					break;
				default:
					JOptionPane.showMessageDialog(null, key + " Opção inválida");
					break;
				} // fim Switch
				
			} else {
				key=0;
				// Fim IF Option
			}
			
		}//Fim while
		
		return key;
		
	} // Fim getMenu()
} // Fim midiaView

