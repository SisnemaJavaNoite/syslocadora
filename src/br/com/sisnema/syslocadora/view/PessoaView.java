package br.com.sisnema.syslocadora.view;

import javax.swing.JOptionPane;
import javax.swing.JTextField;


import br.com.sisnema.syslocadora.model.Pessoa;

public class PessoaView {
	public static void run(){
		
		getMenu();
		
		} //Fim Run()
	
	private static Pessoa cadastrar() {
		Pessoa client = new Pessoa();
		
		JTextField field1 = new JTextField();
		JTextField field2 = new JTextField();
		JTextField field3 = new JTextField();
		
		Object[] message = {
		    "ID:",field1,
		    "Nome:", field2,
		    "Endereço:", field3
		};
		
		int option = JOptionPane.showConfirmDialog(null, message, "Cadastro de Clientes", JOptionPane.OK_CANCEL_OPTION);
		
		if (option == JOptionPane.OK_OPTION) {
		    client.id = field1.getText();
		    client.nome = field2.getText();
		    client.endereco = field3.getText();
		    
		    JOptionPane.showMessageDialog(null, "Cliente cadastrado com sucesso \n" 
					+"ID: " + client.id +"\n"
					+"Nome: " + client.nome +"\n"
					+"Endereço: " + client.endereco +"\n"
					);
		   
		} // Fim IF Option
		
		return client;
		} // fim Class Pessoa Cadastrar()
	
	private static int getMenu() {
		int key = 77;
		
		while (key != 0 ) {
			JTextField field1 = new JTextField();
			Object[] message = {
					"1 - Cadastrar\n",
					"2 - Alterar\n",
					"3 - Excluir\n",
					"4 - Listar\n",
					"0 - Voltar\n",
					"Opção: ", field1
			};
			int option = JOptionPane.showConfirmDialog(null, message,"Menu Clientes",JOptionPane.OK_CANCEL_OPTION);
			
			if (option == JOptionPane.OK_OPTION) {
				
				key = Integer.parseInt(field1.getText());
			
				
				switch (key) {
					case 1:
						cadastrar();
						break;
					case 2:	
						JOptionPane.showMessageDialog(null, "Menu Alterar");
						//alterar();
						break;
					case 3:	
						JOptionPane.showMessageDialog(null, "Menu Excluir");
						//excluir();
						break;
					case 4:	
						JOptionPane.showMessageDialog(null, "Listar");
						//listar();
						break;
					case 0:	
						JOptionPane.showMessageDialog(null, "Abrindo Menu Anterior");
						break;
					default:
						JOptionPane.showMessageDialog(null, key + " Opção inválida");
						break;
				} // fim Switch
				
			} else {
				key=0;
				// Fim IF Option
			}
			
		}//Fim while
		
		return key;
		
	} // getMenu()
} //Fim Class Pessoa View
